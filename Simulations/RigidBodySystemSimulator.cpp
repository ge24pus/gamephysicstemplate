﻿#include "RigidBodySystemSimulator.h"
#include <ctime>
#include <math.h>
RigidBodySystemSimulator::RigidBodySystemSimulator()
{
	rb_sys = new RigidBodySystem();
	gravity = -0.98f;
	currentBody = 0;
	onCorner = false;
	/*swingStep = 5;
	swingingDir = false;
	tick = false;*/
}

const char* RigidBodySystemSimulator::getTestCasesStr() {
	return "";
}

RigidBody* RigidBodySystemSimulator::resetBow() {
	free(wreckingBall);
	rb_sys->rbs.pop_back();
	rb_sys->num_rb -= 1;
	int p = rb_sys->add_sphere(Vec3(0, 0, 0.), 0.3, 0.5, Vec3(0, 1, 0), degree2rad(0));
	wreckingBall = rb_sys->rbs[p];
	return wreckingBall;
}

void RigidBodySystemSimulator::reset() {
	clearAllBoxes();
	currentBody = 0;
	onCorner = false;
	gravity = -0.98f;
	/*swingStep = 0;
	swingingDir = false;
	tick = false;*/
}

void RigidBodySystemSimulator::initUI(DrawingUtilitiesClass* DUC)
{
	this->DUC = DUC;
}

void RigidBodySystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	drawBox();
	drawSphere();
}

RigidBody* RigidBodySystemSimulator::getWreckingBall() {
	return wreckingBall;
}

void RigidBodySystemSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	setDemo();
}

void RigidBodySystemSimulator::externalForcesCalculations(float timeElapsed)
{

}

void RigidBodySystemSimulator::checkHitBoxesTimer(float timeStep) {
	vector<RigidBody*>::iterator start = rb_sys->rbs.begin();
	//std::time_t timeNow = std::time(nullptr);
	for (start;  start != rb_sys->rbs.end();) {
		if ((*start)->isHit) {
			//std::wcout << (*start)->hitTime << "\n";
			if ((*start)->hitTime > 4)
			{
				free(*start);
				start = rb_sys->rbs.erase(start);
				rb_sys->num_rb -= 1;
				continue;
			}
			else {
				(*start)->hitTime += timeStep;
			}
		}
		++start;
	}
}

void RigidBodySystemSimulator::simulateTimestep(float timeStep)
{
	/*if (wreckingBall->isFixed)
	{
		return;
	}*/
	checkHitBoxesTimer(timeStep);
	for (int i = 0; i < rb_sys->num_rb; ++i) {
		RigidBody* rb = rb_sys->rbs[i];
		if (rb->type == SPHERE || (rb->type == BOX && rb->isHit))
		{
			if (!rb->isFixed)
			{
				setNewVelocity(rb, timeStep);
				setNewPosition(rb, timeStep);
			}
			// update rotation
			setNewTorque(rb);
			setNewOrientation(rb, timeStep);
			setNewAngularMoment(rb, timeStep);
			setNewI_tensor(rb);
			setNewAngularVelocity(rb);
			setNewRotation(rb);
		}

		for (int j = 0; j < rb_sys->num_rb; ++j) {
			if (i < j) {
				if (rb->type == BOX && rb_sys->rbs[j]->type == BOX) {
					Mat4 obj2World_A = rb->scaleMat * rb->rotMat * rb->translatMat;
					Mat4 obj2World_B = rb_sys->rbs[j]->scaleMat * rb_sys->rbs[j]->rotMat * rb_sys->rbs[j]->translatMat;
					processCollision(checkCollisionSAT(obj2World_A, obj2World_B), i, j);
				}
				if (rb->type == SPHERE || rb_sys->rbs[j]->type == SPHERE) {
					CollisionInfo info = checkCollisionBoxNSphere(*rb, *rb_sys->rbs[j]);
					processCollision(info, i, j);
				}
			}
		}
		processCollisionWithGround(rb);
		clearAllForcesAndTorque(rb);

		//if (m_iTestCase == 1)
		//{
		//	std::time_t time = std::time(nullptr);
		//	bool dir = (bool)((int)floor(time / 10) % 2);
		//	/*if (dir != swingingDir)
		//	{*/
		//		for (int i = 0; i < rb_sys->num_rb; ++i)
		//		{
		//			RigidBody* rb = rb_sys->rbs[i];
		//			if (rb->type == BOX)
		//			{
		//				rb->l_velocity[0] = 5 * (dir ? 1 : -1);
		//				//rb_sys->set_translatMat(rb);
		//			}
		//		}
		//	//}
		//	/*bool shouldTick = (time % 2) > 0;
		//	if (shouldTick != tick)
		//	{
		//		tick = !tick;
		//		for (int i = 0; i < rb_sys->num_rb; ++i)
		//		{
		//			RigidBody* rb = rb_sys->rbs[i];
		//			if (rb->type == BOX)
		//			{
		//				rb->center[0] += 0.5 * (swingingDir ? 1 : -1);
		//				rb_sys->set_translatMat(rb);
		//			}
		//		}
		//	}*/
		//}
	}
}

int RigidBodySystemSimulator::getNumberOfRigidBodies()
{
	return rb_sys->num_rb;
}

Vec3 RigidBodySystemSimulator::getPositionOfRigidBody(int i) {
	return rb_sys->rbs[i]->center;
}

Vec3 RigidBodySystemSimulator::getLinearVelocityOfRigidBody(int i) {
	return rb_sys->rbs[i]->l_velocity;
}

Vec3 RigidBodySystemSimulator::getAngularVelocityOfRigidBody(int i) {
	return rb_sys->rbs[i]->a_velocity;
}

void RigidBodySystemSimulator::applyForceOnBody(int i, Vec3 loc, Vec3 force) {
	Force f;
	f.loc = loc;
	f.force = force;
	rb_sys->rbs[i]->forces.push_back(f);
}

void RigidBodySystemSimulator::addBox(Vec3 position, Vec3 size, int mass) {
	rb_sys->add_box(position, size, mass);
}

void RigidBodySystemSimulator::addSphere(Vec3 position, float radius, int mass) {
	rb_sys->add_sphere(position, radius, mass);
}

void RigidBodySystemSimulator::setOrientationOf(int i, Quat orientation) {
	rb_sys->rbs[i]->orientation = orientation;
}

void RigidBodySystemSimulator::setVelocityOf(int i, Vec3 velocity) {
	rb_sys->rbs[i]->l_velocity = velocity;
}

// Custom Functions

void RigidBodySystemSimulator::clearAllBoxes() {
	for (int i = 0; i < rb_sys->num_rb; ++i) {
		free(rb_sys->rbs[i]);
	}
	rb_sys->rbs.clear();
	rb_sys->num_rb = 0;
}

void RigidBodySystemSimulator::clearAllForcesAndTorque(RigidBody* rb) {
	rb->forces.clear();
	rb->torque = Vec3();
}

void RigidBodySystemSimulator::setNewVelocity(RigidBody* rb, float timeStep, bool use_gravity) {
	Vec3 F = Vec3();
	for (int i = 0; i < rb->forces.size(); ++i) {
		F += rb->forces[i].force;
	}
	Vec3 a = F / rb->mass;
	if (use_gravity) {
		a += gravity * Vec3(0, 1, 0);
	}
	rb->l_velocity += a * timeStep;
}

void RigidBodySystemSimulator::setNewPosition(RigidBody* rb, float timeStep) {
	rb->center += rb->l_velocity * timeStep;
	rb_sys->set_translatMat(rb);
}

void RigidBodySystemSimulator::setNewTorque(RigidBody* rb) {
	for (int i = 0; i < rb->forces.size(); ++i) {
		rb->torque += cross(rb->forces[i].loc, rb->forces[i].force);
	}
}

void RigidBodySystemSimulator::setNewOrientation(RigidBody* rb, float timeStep) {
	rb->orientation += (timeStep / 2) * Quat(rb->a_velocity[0], rb->a_velocity[1], rb->a_velocity[2], 0.) * rb->orientation;
	rb->orientation = rb->orientation.unit();
	rb_sys->set_rotMat(rb);
}

void RigidBodySystemSimulator::setNewAngularMoment(RigidBody* rb, float timeStep) {
	rb->angular_moment += timeStep * rb->torque;
}

void RigidBodySystemSimulator::setNewI_tensor(RigidBody* rb) {
	Mat4 transpose = rb->rotMat;
	transpose.transpose();
	rb->i_tensor = rb->rotMat * rb->i_tensor * transpose;
}

void RigidBodySystemSimulator::setNewAngularVelocity(RigidBody* rb) {
	rb->a_velocity = rb->i_tensor.inverse() * rb->angular_moment;
}

void RigidBodySystemSimulator::setNewRotation(RigidBody* rb) {
	if (norm(rb->a_velocity) != 0.) {
		rb_sys->set_rotation(rb);
	}
}

void RigidBodySystemSimulator::processCollisionWithGround(RigidBody* rb) {
	Vec3 normal = Vec3(0, 1, 0);
	if (rb->type == BOX) {
		for (int i = 0; i < rb->vertices.size(); ++i) {
			if ((rb->vertices[i] + rb->center).y <= -1.) {
				//float J = 1;
				//rb->angular_moment += cross(rb->vertices[i], J * normal);
				//rb->l_velocity += J * normal / rb->mass;
				rb->l_velocity = 0.5 * Vec3(rb->l_velocity.x, -1 * rb->l_velocity.y, rb->l_velocity.z);
				rb->angular_moment *= -0.2;
				rb->center += normal * (-1 - (rb->vertices[i] + rb->center).y);
			}
		}
	}
	if (rb->type == SPHERE) {
		if (rb->center.y - rb->size[0] <= -1.) {
			rb->l_velocity = 0.5 * Vec3(rb->l_velocity.x, -1 * rb->l_velocity.y, rb->l_velocity.z);
			rb->center += normal * (-1 - rb->center.y + rb->size[0]);
		}
	}
}
void RigidBodySystemSimulator::markForSelfDestruct(int a, int b) {
	RigidBody* first = rb_sys->rbs[a];
	RigidBody* second = rb_sys->rbs[b];
	if (first->type == BOX && !first->isHit)
	{
		first->isHit = true;
		first->hitTime = 0;
	}
	if (second->type == BOX && !second->isHit)
	{
		second->isHit = true;
		second->hitTime = 0;
	}
}

void RigidBodySystemSimulator::processCollision(CollisionInfo collisionInfo, int A, int B) {
	if (collisionInfo.isValid) {
		markForSelfDestruct(A, B);
		Vec3 collision = collisionInfo.collisionPointWorld;
		Vec3 normal = collisionInfo.normalWorld;

		RigidBody* rbA = rb_sys->rbs[A];
		RigidBody* rbB = rb_sys->rbs[B];

		Vec3 xa = world2local(rbA, collision);
		Vec3 xb = world2local(rbB, collision);

		// Vrel, the relative velocity between A and B at the collision point(in world space).
		Vec3 Vrel = (rbA->l_velocity + cross(rbA->a_velocity, xa)) - (rbB->l_velocity + cross(rbB->a_velocity, xb));
		// If Vrel ¡¤ n > 0, this indicates that the bodies are separating.
		if (dot(Vrel, normal) > 0) {
			if (collisionInfo.depth > 0.) {
				rbA->center += (normal * collisionInfo.depth) / 2;
				rbB->center -= (normal * collisionInfo.depth) / 2;
			}
		}
		// Otherwise continue to calculate the impulse J, and apply it to both bodies.
		else {
			float c = 1.;									// bounciness
			float J = (-(1. + c) * dot(Vrel, normal)) /		// impulse
				((1. / rbA->mass) + (1. / rbB->mass) +
					dot(cross(rbA->i_tensor.inverse() * (cross(xa, normal)), xa) + cross(rbB->i_tensor.inverse() * (cross(xb, normal)), xb), normal));

			// Since a same collision will be detected twice as it belongs to both of the objects, we need to add half of the result
			rbA->l_velocity += J * normal / rbA->mass;
			rbB->l_velocity -= J * normal / rbB->mass;

			rbA->angular_moment += cross(xa, J * normal);
			rbB->angular_moment -= cross(xb, J * normal);

			if (collisionInfo.depth > 0.) {
				rbA->center += (normal * collisionInfo.depth) / 2;
				rbB->center -= (normal * collisionInfo.depth) / 2;
			}
		}
	}
}

void RigidBodySystemSimulator::drawBox() {
	// set the lightings, like in the mass spring exercise
	DUC->setUpLighting(Vec3(0, 0, 0), 0.4 * Vec3(1, 1, 1), 2000.0, Vec3(0.5, 0.5, 0.5));
	for (int i = 0; i < rb_sys->num_rb; ++i) {
		// input matrix is the transfer matrix from object space of the box to the world space
		RigidBody* rb = rb_sys->rbs[i];
		if (rb->type == BOX) {
			Mat4 Obj2WorldMatrix = rb->scaleMat * rb->rotMat * rb->translatMat;
			DUC->drawRigidBody(Obj2WorldMatrix.toDirectXMatrix());
		}
	}
}

void RigidBodySystemSimulator::drawSphere() {
	// set the lightings, like in the mass spring exercise
	DUC->setUpLighting(Vec3(0, 0, 0), 0.4 * Vec3(1, 1, 1), 2000.0, Vec3(0.5, 0.5, 0.5));
	for (int i = 0; i < rb_sys->num_rb; ++i) {
		// input matrix is the transfer matrix from object space of the box to the world space
		RigidBody* rb = rb_sys->rbs[i];
		if (rb->type == SPHERE) {
			Mat4 Obj2WorldMatrix = rb->scaleMat * rb->rotMat * rb->translatMat;
			DUC->drawSphere(rb->center, rb->size[0]);
		}
	}
}

float RigidBodySystemSimulator::degree2rad(float degree) {
	return degree * M_PI / 180;
}

Vec3 RigidBodySystemSimulator::world2local(RigidBody* rb, Vec3 loc) {
	return loc - rb->center;
}

void RigidBodySystemSimulator::setDemo()
{
	clearAllBoxes();
	int numBoxes = 20;
	int numBoxesInRow = 5;
	float zStart = 5;
	float sideLength = 0.3;
	float gridSpace = 0.15;
	float xRange = (numBoxesInRow-1) * (sideLength+gridSpace);

	for (size_t i = 0; i < numBoxes/numBoxesInRow; i++)
	{
		float x = -xRange / 2;
		for (size_t j = 0; j < numBoxesInRow; j++)
		{
			rb_sys->add_box(Vec3(x, 0, zStart + i*(sideLength+gridSpace)), Vec3(sideLength, sideLength, sideLength), 20., Vec3(0, 1, 0), degree2rad(0));
			x += sideLength + gridSpace;
		}
	}
	
	/*int i = rb_sys->add_box(Vec3(-0.6, 0, 5), Vec3(0.3, 0.3, 0.3), 20., Vec3(0, 1, 0), degree2rad(0));
	int j = rb_sys->add_box(Vec3(0, 0, 5), Vec3(0.3, 0.3, 0.3), 20., Vec3(0, 1, 0), degree2rad(0));
	int k = rb_sys->add_box(Vec3(0.6, 0, 5), Vec3(0.3, 0.3, 0.3), 20., Vec3(0, 1, 0), degree2rad(0));
	int l = rb_sys->add_box(Vec3(-0.9, 0, 5.5), Vec3(0.3, 0.3, 0.3), 20., Vec3(0, 1, 0), degree2rad(0));
	int m = rb_sys->add_box(Vec3(-0.3, 0, 5.5), Vec3(0.3, 0.3, 0.3), 20., Vec3(0, 1, 0), degree2rad(0));
	int n = rb_sys->add_box(Vec3(0.3, 0, 5.5), Vec3(0.3, 0.3, 0.3), 20., Vec3(0, 1, 0), degree2rad(0));
	int o = rb_sys->add_box(Vec3(0.9, 0, 5.5), Vec3(0.3, 0.3, 0.3), 20., Vec3(0, 1, 0), degree2rad(0));*/

	int p = rb_sys->add_sphere(Vec3(0, 0, 0.), 0.3, 0.5, Vec3(0, 1, 0), degree2rad(0));
	wreckingBall = rb_sys->rbs[p];
}

void RigidBodySystemSimulator::onClick(int x, int y)
{
	
}

void RigidBodySystemSimulator::onMouse(int x, int y)
{

}
