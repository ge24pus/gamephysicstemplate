#ifndef RIGIDBODYSYSTEM_h
#define RIGIDBODYSYSTEM_h
#include "Simulator.h"
#include <ctime>

#define BOX 0
#define SPHERE 1

struct MassPoint {
	Vec3 position;
	Vec3 velocity;
	Vec3 acceleration;
	bool isFixed;
};

struct Spring {
	int masspoint1;
	int masspoint2;
	float initialLength;
};

struct Force {
	Vec3 loc;				// where the force applies (relative position)
	Vec3 force;
};

struct RigidBody {
	int type;				// box or sphere
	Vec3 center;			// center of the body
	Vec3 size;				// length / width / height for box, radius / radius / radius for sphere
	float mass;				// mass of the body

	vector<Vec3> vertices;  // vertices of the box (relative position set in add_rb)

	Quat orientation;		// orientation of the body

	Mat4 i_tensor;			// inertia tensor
	Vec3 angular_moment;	// angular momentum

	Vec3 l_velocity;		// linear velocity
	Vec3 a_velocity;		// angular velocity

	vector<Force> forces;	// forces applied
	Vec3 torque;			// torque

	Mat4 scaleMat;			// scale matrix
	Mat4 rotMat;			// rotation matrix
	Mat4 translatMat;		// translation matrix
	bool isFixed;
	bool isHit;
	float hitTime;
};

class RigidBodySystem
{
public:
	// Attributes
	int num_rb;				// number of rigid bodies in the system
	vector<RigidBody*> rbs;	// collection of the rigid bodies (their pointer)

	// Construtors
	RigidBodySystem() {
		num_rb = 0;
	};
	
	// Functions
	void set_vertices(RigidBody* rb) {
		rb->vertices.clear();
		Vec3 size = rb->size;
		rb->vertices.push_back(Vec3( size[0] / 2,  size[1] / 2,  size[2] / 2));
		rb->vertices.push_back(Vec3( size[0] / 2, -size[1] / 2,  size[2] / 2));
		rb->vertices.push_back(Vec3( size[0] / 2,  size[1] / 2, -size[2] / 2));
		rb->vertices.push_back(Vec3(-size[0] / 2,  size[1] / 2,  size[2] / 2));
		rb->vertices.push_back(Vec3(-size[0] / 2, -size[1] / 2,  size[2] / 2));
		rb->vertices.push_back(Vec3( size[0] / 2, -size[1] / 2, -size[2] / 2));
		rb->vertices.push_back(Vec3(-size[0] / 2,  size[1] / 2, -size[2] / 2));
		rb->vertices.push_back(Vec3(-size[0] / 2, -size[1] / 2, -size[2] / 2));
	}

	void precompute_I0(RigidBody* rb) {
		rb->i_tensor = Mat4();
		for (int i = 0; i < 8; ++i) {
			rb->i_tensor += Mat4( rb->vertices[i][1] * rb->vertices[i][1] * rb->vertices[i][2] * rb->vertices[i][2], -rb->vertices[i][0] * rb->vertices[i][1], -rb->vertices[i][0] * rb->vertices[i][2], 0.,
								 -rb->vertices[i][0] * rb->vertices[i][1],  rb->vertices[i][0] * rb->vertices[i][0] * rb->vertices[i][2] * rb->vertices[i][2], -rb->vertices[i][1] * rb->vertices[i][2], 0.,
								 -rb->vertices[i][0] * rb->vertices[i][2], -rb->vertices[i][1] * rb->vertices[i][2],  rb->vertices[i][0] * rb->vertices[i][0] * rb->vertices[i][1] * rb->vertices[i][1], 0.,
																	   0.,										 0.,									   0.,									1./rb->mass) * (rb->mass / 8);
		}
	}

	void set_scaleMat(RigidBody* rb) {
		rb->scaleMat = Mat4(rb->size[0],		  0.,		   0., 0.,
									 0., rb->size[1],		   0., 0.,
									 0.,		  0., rb->size[2], 0.,
									 0.,		  0.,		   0., 1.);
	}

	void set_rotMat(RigidBody* rb) {
		rb->rotMat = rb->orientation.getRotMat();
	}

	void set_rotation(RigidBody* rb) {
		set_vertices(rb);
		for (int i = 0; i < rb->vertices.size(); ++i) {
			rb->vertices[i] = rb->rotMat.transformVector(rb->vertices[i]);
		}
	}

	void set_translatMat(RigidBody* rb) {
		rb->translatMat.initTranslation(rb->center[0], rb->center[1], rb->center[2]);
	}

	void set_i_tensor(RigidBody* rb) {
		Mat4 transpose = rb->rotMat;
		transpose.transpose();
		rb->i_tensor = rb->rotMat * rb->i_tensor * transpose;
	}

	int add_box(Vec3 center, Vec3 size, float mass, Vec3 axis = Vec3(1, 0, 0), float angle = 0.) {
		RigidBody* rb = new RigidBody;
		rb->type = BOX;
		rb->center = center;
		rb->size = size;
		rb->mass = mass;
		rb->orientation = Quat(axis, angle);
		rb->orientation = rb->orientation.unit();
		rb->isFixed = false;
		rb->isHit = false;
		rb->hitTime = 0;

		set_vertices(rb);
		precompute_I0(rb);

		rb->l_velocity = Vec3();
		rb->a_velocity = Vec3();

		rb->torque = Vec3();
		rb->angular_moment = Vec3();

		set_scaleMat(rb);
		set_rotMat(rb);
		set_rotation(rb);
		set_translatMat(rb);

		set_i_tensor(rb);

		rbs.push_back(rb);
		num_rb = rbs.size();
		return num_rb - 1;
	};

	int add_sphere(Vec3 center, float radius, float mass, Vec3 axis = Vec3(1, 0, 0), float angle = 0.) {
		RigidBody* rb = new RigidBody;
		rb->type = SPHERE;
		rb->center = center;
		rb->size = Vec3(radius);
		rb->mass = mass;
		rb->orientation = Quat(axis, angle);
		rb->orientation = rb->orientation.unit();
		rb->isFixed = true;
		rb->isHit = false;
		rb->hitTime = 0;
		set_vertices(rb);
		precompute_I0(rb);

		rb->l_velocity = Vec3();
		rb->a_velocity = Vec3();

		rb->torque = Vec3();
		rb->angular_moment = Vec3();

		set_scaleMat(rb);
		set_rotMat(rb);
		set_rotation(rb);
		set_translatMat(rb);

		set_i_tensor(rb);

		rbs.push_back(rb);
		num_rb = rbs.size();
		return num_rb - 1;
	};
};
#endif

 