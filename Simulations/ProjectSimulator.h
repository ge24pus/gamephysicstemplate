#ifndef PROJECT_h
#define PROJECT_h
#include "Simulator.h"
#include "MassSpringSystemSimulator.h"
#include "RigidBodySystemSimulator.h"

class ProjectSimulator :public Simulator {
public:
	// Construtors
	ProjectSimulator();

	// UI Functions
	const char* getTestCasesStr();
	void initUI(DrawingUtilitiesClass* DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);

	void applyExternalForce(Vec3 force);

	//void positionCorrection();


private:
	MassSpringSystemSimulator* massSpringSystem;
	RigidBodySystemSimulator * rigidBodySystem;
	bool resetBow;
	int numTrials;
};
#endif
