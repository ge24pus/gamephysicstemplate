#include "collisionDetect.h"
#include "Structs.h"

/* params:
sphere: Sphere
obj2World_B: Box
*/
inline CollisionInfo checkCollisionBoxNSphere(RigidBody sphere, RigidBody box) {
	CollisionInfo info;
	info.isValid = false;
	if (sphere.type == BOX && box.type == SPHERE) {
		RigidBody tempRb = box;
		box = sphere;
		sphere = tempRb;

		// get box's maxs and mins
		float xmin = 1000., ymin = 1000., zmin = 1000., xmax = -1000., ymax = -1000., zmax = -1000.;
		for (int i = 0; i < box.vertices.size(); ++i) {
			if (box.vertices[i].x < xmin) {
				xmin = box.vertices[i].x;
			}
			if (box.vertices[i].y < ymin) {
				ymin = box.vertices[i].y;
			}
			if (box.vertices[i].z < zmin) {
				zmin = box.vertices[i].z;
			}
			if (box.vertices[i].x > xmax) {
				xmax = box.vertices[i].x;
			}
			if (box.vertices[i].y > ymax) {
				ymax = box.vertices[i].y;
			}
			if (box.vertices[i].z > zmax) {
				zmax = box.vertices[i].z;
			}
		}
		// cout << xmin << " " << ymin << " " << zmin << " " << xmax << " " << ymax << " " << zmax << "\n";
		// get box closest point to sphere center by clamping
		Vec3 closest;
		float temp;
		Vec3 spherePos = sphere.center - box.center;
		if (spherePos.x < xmax) { temp = spherePos.x; }
		else { temp = xmax; }
		if (xmin > temp) { closest.x = xmin; }
		else { closest.x = temp; }

		if (spherePos.y < ymax) { temp = spherePos.y; }
		else { temp = ymax; }
		if (ymin > temp) { closest.y = ymin; }
		else { closest.y = temp; }

		if (spherePos.z < zmax) { temp = spherePos.z; }
		else { temp = zmax; }
		if (zmin > temp) { closest.z = zmin; }
		else { closest.z = temp; }

		// if (sphere.center.x < xmax) { temp = sphere.center.x; }
		// else { temp = xmax; }
		// if (xmin > temp) { closest.x = xmin; }
		// else { closest.x = temp; }
		// 
		// if (sphere.center.y < ymax) { temp = sphere.center.y; }
		// else { temp = ymax; }
		// if (ymin > temp) { closest.y = ymin; }
		// else { closest.y = temp; }
		// 
		// if (sphere.center.z < zmax) { temp = sphere.center.z; }
		// else { temp = zmax; }
		// if (zmin > temp) { closest.z = zmin; }
		// else { closest.z = temp; }

		// test if collide
		float distance = sqrt(pow((closest.x - spherePos.x), 2) + pow((closest.y - spherePos.y), 2) + pow((closest.z - spherePos.z), 2));
		// cout << "closest: " << closest << " distance: " << distance << "\n";
		// intersects
		if (distance <= sphere.size[0]) {
			info.isValid = true;
			info.collisionPointWorld = closest + box.center;
			info.depth = sphere.size[0] - distance;
			info.normalWorld = (info.collisionPointWorld - sphere.center) / norm(sphere.center - info.collisionPointWorld);
			return info;
		}
		else {
			info.collisionPointWorld = closest;
			return info;
		}
	}
	if (sphere.type == SPHERE && box.type == BOX) {
		// get box's maxs and mins
		float xmin = 1000., ymin = 1000., zmin = 1000., xmax = -1000., ymax = -1000., zmax = -1000.;
		for (int i = 0; i < box.vertices.size(); ++i) {
			if (box.vertices[i].x < xmin) {
				xmin = box.vertices[i].x;
			}
			if (box.vertices[i].y < ymin) {
				ymin = box.vertices[i].y;
			}
			if (box.vertices[i].z < zmin) {
				zmin = box.vertices[i].z;
			}
			if (box.vertices[i].x > xmax) {
				xmax = box.vertices[i].x;
			}
			if (box.vertices[i].y > ymax) {
				ymax = box.vertices[i].y;
			}
			if (box.vertices[i].z > zmax) {
				zmax = box.vertices[i].z;
			}
		}
		// cout << xmin << " " << ymin << " " << zmin << " " << xmax << " " << ymax << " " << zmax << "\n";
		// get box closest point to sphere center by clamping
		Vec3 closest;
		float temp;
		Vec3 spherePos = sphere.center - box.center;
		if (spherePos.x < xmax) { temp = spherePos.x; }
		else { temp = xmax; }
		if (xmin > temp) { closest.x = xmin; }
		else { closest.x = temp; }

		if (spherePos.y < ymax) { temp = spherePos.y; }
		else { temp = ymax; }
		if (ymin > temp) { closest.y = ymin; }
		else { closest.y = temp; }

		if (spherePos.z < zmax) { temp = spherePos.z; }
		else { temp = zmax; }
		if (zmin > temp) { closest.z = zmin; }
		else { closest.z = temp; }

		// test if collide
		float distance = sqrt(pow((closest.x - spherePos.x), 2) + pow((closest.y - spherePos.y), 2) + pow((closest.z - spherePos.z), 2));
		// cout << "closest: " << closest << " distance: " << distance << "\n";
		// intersects
		if (distance <= sphere.size[0]) {
			info.isValid = true;
			info.collisionPointWorld = closest + box.center;
			info.depth = sphere.size[0] - distance;
			info.normalWorld = (sphere.center - info.collisionPointWorld) / norm(sphere.center - info.collisionPointWorld);
			return info;
		}
		else {
			info.collisionPointWorld = closest;
			return info;
		}
	}

}