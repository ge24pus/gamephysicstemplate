#ifndef MASSSPRINGSYSTEMSIMULATOR_h
#define MASSSPRINGSYSTEMSIMULATOR_h
#include "Simulator.h"
#include "Structs.h"

#define MASS 10.0f
#define STIFFNESS 40.0f
#define DAMPING 0.5f

class MassSpringSystemSimulator :public Simulator {
public:
	// Construtors
	MassSpringSystemSimulator();

	const char* getTestCasesStr();
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void simulateTimestep(float timeStep);
	void notifyCaseChanged(int testCase);
	void initUI(DrawingUtilitiesClass* DUC);
	void onClick(int x, int y);
	void onMouse(int x, int y);
	void applyExternalForce(Vec3 force);
	void externalForcesCalculations(float timeElapsed);

	// Specific Functions
	void setMass(float mass);
	void setStiffness(float stiffness);
	void setDampingFactor(float damping);

	int addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed);
	void addSpring(int masspoint1, int masspoint2, float initialLength);
	int getNumberOfMassPoints();
	int getNumberOfSprings();
	Vec3 getPositionOfMassPoint(int index);
	Vec3 getVelocityOfMassPoint(int index);
	void setWreckingBall(RigidBody* rb);
	void resetBow();
private:
	// Data Attributes
	float m_fMass;
	float m_fStiffness;
	float m_fDamping;
	bool detached;
	bool fired;
	bool clicked;
	bool enable_gravity;
	bool enable_damping;
	bool enable_reflect;
	float gravityAccel;
	float displacement;
	// UI Attributes
	Vec3 m_externalForce;
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;

	std::vector<MassPoint> massPoints;
	std::vector<Spring> springs;
	RigidBody* wreckingBall;

	void stepMidpoint(float timeStep);
	void checkPositionAndDetach();
	void initDemo();
	MassPoint stepEuler(float timeStep);
};
#endif