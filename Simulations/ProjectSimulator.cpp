#include "ProjectSimulator.h"
#include <windows.h>

ProjectSimulator::ProjectSimulator()
{
	this->massSpringSystem = new MassSpringSystemSimulator();
	this->rigidBodySystem = new RigidBodySystemSimulator();
	resetBow = false;
	numTrials = 1;
}

const char* ProjectSimulator::getTestCasesStr() {
	return "Static,Moving";
}

void ProjectSimulator::reset() {
	resetBow = false;
	numTrials = 1;
	massSpringSystem->reset();
	rigidBodySystem->reset();
}

void ProjectSimulator::initUI(DrawingUtilitiesClass* DUC)
{
	this->DUC = DUC;
	TwAddVarRW(DUC->g_pTweakBar, "Reset Bow", TW_TYPE_BOOLCPP, &resetBow, "");
	massSpringSystem->initUI(DUC);
	rigidBodySystem->initUI(DUC);
}

void ProjectSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	//std::mt19937 eng;
	//std::uniform_real_distribution<float> randCol(0.0f, 1.0f);
	// draw world

	// draw mass and rigid bodies
	if (resetBow)
	{
		RigidBody* ball = rigidBodySystem->resetBow();
		massSpringSystem->resetBow();
		massSpringSystem->setWreckingBall(ball);
		resetBow = false;
		numTrials += 1;
	}
	massSpringSystem->drawFrame(pd3dImmediateContext);
	rigidBodySystem->drawFrame(pd3dImmediateContext);
}

void ProjectSimulator::applyExternalForce(Vec3 force)
{
	//massSpringSystem->applyExternalForce(force);
}

void ProjectSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	reset();
	rigidBodySystem->notifyCaseChanged(testCase);
	massSpringSystem->notifyCaseChanged(testCase);
	massSpringSystem->setWreckingBall(rigidBodySystem->getWreckingBall());
}

void ProjectSimulator::externalForcesCalculations(float timeElapsed)
{
	massSpringSystem->externalForcesCalculations(timeElapsed);
}

void ProjectSimulator::simulateTimestep(float timeStep)
{
	if (rigidBodySystem->getNumberOfRigidBodies() == 1)
	{
		std::string text = "Done in " + std::to_string(numTrials) + " shots!";
		MessageBoxA(0, text.c_str(), "", MB_OK);

		// reset game
		notifyCaseChanged(m_iTestCase);
		return;
	}
	massSpringSystem->simulateTimestep(timeStep);
	rigidBodySystem->simulateTimestep(timeStep);
}


//
//void ProjectSimulator::positionCorrection()
//{
//	for (int i = 0; i < massPoints.size(); i++)
//	{
//		if (massPoints[i]->pos[1] < -1)
//		{
//			massPoints[i]->pos[1] = -1;
//			massPoints[i]->v *= -0.5;
//		}
//	}
//}

void ProjectSimulator::onClick(int x, int y)
{
	massSpringSystem->onClick(x, y);
}

void ProjectSimulator::onMouse(int x, int y)
{
	massSpringSystem->onMouse(x, y);
}
