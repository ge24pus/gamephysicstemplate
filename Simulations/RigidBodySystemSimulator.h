#ifndef RIGIDBODYSYSTEMSIMULATOR_h
#define RIGIDBODYSYSTEMSIMULATOR_h
#include "Simulator.h"
#include "collisionDetect_sphere.h"

class RigidBodySystemSimulator :public Simulator {
public:
	// Construtors
	RigidBodySystemSimulator();

	const char* getTestCasesStr();
	void initUI(DrawingUtilitiesClass* DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void simulateTimestep(float timeStep);
	void externalForcesCalculations(float timeElapsed);
	void onClick(int x, int y);
	void onMouse(int x, int y);

	// ExtraFunctions
	int getNumberOfRigidBodies();
	Vec3 getPositionOfRigidBody(int i);
	Vec3 getLinearVelocityOfRigidBody(int i);
	Vec3 getAngularVelocityOfRigidBody(int i);
	void applyForceOnBody(int i, Vec3 loc, Vec3 force);
	void addBox(Vec3 position, Vec3 size, int mass);
	void addSphere(Vec3 position, float radius, int mass);
	void setOrientationOf(int i, Quat orientation);
	void setVelocityOf(int i, Vec3 velocity);

	// Custom Functions
	void clearAllBoxes();

	void clearAllForcesAndTorque(RigidBody* rb);						// Clear forces

	// update position
	void setNewVelocity(RigidBody* rb, float timeStep, bool use_gravity = false);	// Compute acceleration with accumulated forces (Newton's second law) and update velocity
	void setNewPosition(RigidBody* rb, float timeStep);					// Update position with velocity

	// update rotation
	void setNewTorque(RigidBody* rb);
	void setNewOrientation(RigidBody* rb, float timeStep);
	void setNewAngularMoment(RigidBody* rb, float timeStep);
	void setNewI_tensor(RigidBody* rb);
	void setNewAngularVelocity(RigidBody* rb);
	void setNewRotation(RigidBody* rb);

	// handle collision
	void processCollision(CollisionInfo collisionInfo, int A, int B);
	void processCollisionWithGround(RigidBody* rb);

	void drawBox();
	void drawSphere();
	float degree2rad(float degree);
	Vec3 world2local(RigidBody* rb, Vec3 loc);

	void setDemo();
	RigidBody* getWreckingBall();
	RigidBody* resetBow();
	void markForSelfDestruct(int a, int b);
	void checkHitBoxesTimer(float timeStep);
private:
	// Attributes
	RigidBodySystem* rb_sys;
	RigidBody* wreckingBall;
	int currentBody;
	bool onCorner;
	float gravity;
	/*int swingStep;
	bool swingingDir;
	bool tick;*/
};
#endif