#include "MassSpringSystemSimulator.h"
#include <math.h>
#include <iostream>
#include<algorithm> 
MassSpringSystemSimulator::MassSpringSystemSimulator() {
	this->enable_gravity = false;
	this->enable_damping = true;
	this->enable_reflect = false;
	this->gravityAccel = -0.98f;
	this->detached = false;
	this->fired = false;
	this->clicked = false;
	displacement = 0.0f;
}

const char* MassSpringSystemSimulator::getTestCasesStr() {
	return "";
}

void MassSpringSystemSimulator::setWreckingBall(RigidBody* rb) {
	this->wreckingBall = rb;
}

void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass* DUC) {
	this->DUC = DUC;
}

void MassSpringSystemSimulator::reset() {
	this->massPoints.clear();
	this->springs.clear();

	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}

void MassSpringSystemSimulator::initDemo() {
	detached = false;
	fired = false;
	clicked = false;
	displacement = 0.0f;
	this->setMass(MASS);
	this->setStiffness(STIFFNESS);
	this->setDampingFactor(DAMPING);
	this->enable_damping = true;

	reset();
	this->addMassPoint({ -1.0f,0.0f,0.0f }, { 0.0f, 0.0f, 0.0f }, true);
	this->addMassPoint({ 1.0f,0.0f,0.0f }, { 0.0f, 0.0f, 0.0f }, true);
	this->addSpring(0, 1, 1.0f);
	this->addSpring(2, 1, 1.0f);
}

void MassSpringSystemSimulator::resetBow() {
	detached = false;
	fired = false;
	clicked = false;
	displacement = 0.0f;
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;

	this->springs.clear();
	this->addSpring(0, 1, 1.0f);
	this->addSpring(2, 1, 1.0f);
}

void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext) {
	std::mt19937 eng;
	std::uniform_real_distribution<float> randCol(0.0f, 1.0f);
	float throttle = abs(displacement);
	throttle = fmin(1, throttle);
	Vec3 color = Vec3((1-throttle) *255, throttle*255, 0);
	for (int i = 0; i < this->massPoints.size(); i++)
	{
		DUC->setUpLighting(Vec3(), 0.4 * Vec3(1, 1, 1), 100, 0.6 * color);
		DUC->drawSphere(this->getPositionOfMassPoint(i), Vec3(0.1f, 0.1f, 0.1f));
	}
	if (!detached)
	{
		DUC->beginLine();
		for (int i = 0; i < this->springs.size(); i++)
		{
			DUC->drawLine(this->getPositionOfMassPoint(i), Vec3(randCol(eng), randCol(eng), randCol(eng)),
				wreckingBall->center, Vec3(randCol(eng), randCol(eng), randCol(eng)));
		}
		DUC->endLine();
	}
}

int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed) {
	MassPoint point = {
		position,
		Velocity,
		Vec3(),
		isFixed
	};
	
	this->massPoints.push_back(point);
	return this->massPoints.size() - 1;
}

Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index) {
	if (index < this->massPoints.size()) {
		return this->massPoints.at(index).position;
	}
	else {
		return { 0,0,0 };
	}
}

Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index) {
	if (index < this->massPoints.size()) {
		return this->massPoints.at(index).velocity;
	}
	else {
		return { 0,0,0 };
	}
}

int MassSpringSystemSimulator::getNumberOfMassPoints() {
	return this->massPoints.size();
}

void MassSpringSystemSimulator::notifyCaseChanged(int testCase) {
	initDemo();
}

void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed) {
	// Apply the mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	if (!fired && wreckingBall != NULL)
	{
		Point2D mouseDiff;
		mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
		mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
		if ((mouseDiff.x != 0 || mouseDiff.y != 0))
		{
			/*Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
			worldViewInv = worldViewInv.inverse();*/
			Vec3 inputView = Vec3((float)mouseDiff.x, 0, (float)-mouseDiff.y);
			//Vec3 inputWorld = worldViewInv.transformVectorNormal(inputView);
			// find a proper scale!
			float inputScale = 0.00001f;
			//inputWorld = inputWorld * inputScale;
			wreckingBall->center += inputView * inputScale;
			wreckingBall->translatMat.initTranslation(wreckingBall->center[0], wreckingBall->center[1], wreckingBall->center[2]);

			displacement = wreckingBall->center[2];
		}
	}
}

void MassSpringSystemSimulator::simulateTimestep(float timeStep) {
	if (!fired || detached || wreckingBall == NULL)
	{
		return;
	}
	else
	{
		this->stepMidpoint(timeStep);
		this->checkPositionAndDetach();
	}
}
void MassSpringSystemSimulator::checkPositionAndDetach() {
	Vec3 point = getPositionOfMassPoint(0);
	if (wreckingBall->center.z - wreckingBall->size[2] > point.z)
	{
		this->springs.clear();
		detached = true;
		wreckingBall->isFixed = false;
	}
}

MassPoint MassSpringSystemSimulator::stepEuler(float timeStep) {
	Vec3 innerForces = Vec3();
	for (size_t j = 0; j < this->springs.size(); j++)
	{
		Spring connection = this->springs.at(j);
		MassPoint fixed = massPoints.at(j);
		Vec3 distance = fixed.position - wreckingBall->center;
		float l2norm = sqrt(pow(distance.x, 2) + pow(distance.y, 2) + pow(distance.z, 2));
		//Vec3 distanceNorm = connection.masspoint1 != index ? distance / l2norm : -1 * distance / l2norm;
		Vec3 distanceNorm = distance / l2norm;

		Vec3 hooke = -1 * this->m_fStiffness * (connection.initialLength - l2norm) * distanceNorm;
		innerForces += hooke;
	}

	Vec3 gravityAccel = this->enable_gravity ? Vec3(0.0f, this->gravityAccel, 0.0f) : Vec3();
	Vec3 damping = this->enable_damping ? -1 * this->m_fDamping * wreckingBall->l_velocity : Vec3();

	Vec3 oldAccel = (innerForces + damping) / wreckingBall->mass + gravityAccel;

	Vec3 newPos = wreckingBall->isFixed ? wreckingBall->center : wreckingBall->center + timeStep * wreckingBall->l_velocity;
	Vec3 newVel = wreckingBall->isFixed ? wreckingBall->l_velocity : wreckingBall->l_velocity + timeStep * oldAccel;

	MassPoint result = {
		newPos,
		newVel,
		Vec3(),
		wreckingBall->isFixed
	};

	return result;
}

void MassSpringSystemSimulator::stepMidpoint(float timeStep) {

	Vec3 innerForces = Vec3();
	MassPoint ballMidPoint = this->stepEuler(timeStep / 2);
	for (size_t j = 0; j < this->springs.size(); j++)
	{
		Spring connection = this->springs.at(j);
		MassPoint mid1 = massPoints.at(j);

		Vec3 distance = mid1.position - ballMidPoint.position;
		float l2norm = sqrt(pow(distance.x, 2) + pow(distance.y, 2) + pow(distance.z, 2));
		//Vec3 distanceNorm = connection.masspoint1 != 1 ? distance / l2norm : -1 * distance / l2norm;
		Vec3 distanceNorm =  distance / l2norm;

		Vec3 hooke = -1 * this->m_fStiffness * (connection.initialLength - l2norm) * distanceNorm;
		innerForces += hooke;
	}

	Vec3 gravityAccel = this->enable_gravity ? Vec3(0.0f, this->gravityAccel, 0.0f) : Vec3();
	Vec3 damping = this->enable_damping ? -1 * this->m_fDamping * wreckingBall->l_velocity : Vec3();

	Vec3 midAccel = (innerForces + damping) / wreckingBall->mass + gravityAccel;

	Vec3 newPos = wreckingBall->center + timeStep * ballMidPoint.velocity;
	Vec3 newVel = wreckingBall->l_velocity + timeStep * midAccel;

	wreckingBall->center = newPos;
	wreckingBall->translatMat.initTranslation(wreckingBall->center[0], wreckingBall->center[1], wreckingBall->center[2]);
	wreckingBall->l_velocity = newVel;
}

void MassSpringSystemSimulator::onClick(int x, int y) {
	if (!clicked)
	{
		m_oldtrackmouse.x = x;
		m_oldtrackmouse.y = y;
	}
	m_trackmouse.x = x;
	m_trackmouse.y = y;
	clicked = true;
}

void MassSpringSystemSimulator::onMouse(int x, int y) {
	if (!clicked || fired)
	{
		return;
	}
	int diffY = m_trackmouse.y - m_oldtrackmouse.y;
	if (displacement > -1)
	{
		m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
		m_trackmouse.x = m_trackmouse.y = 0;
		clicked = false;
		return;
	}
	fired = true;
}

void MassSpringSystemSimulator::setMass(float mass) {
	this->m_fMass = mass;
}

void MassSpringSystemSimulator::setStiffness(float stiffness) {
	this->m_fStiffness = stiffness;
}

void MassSpringSystemSimulator::setDampingFactor(float damping) {
	this->m_fDamping = damping;
}

void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength) {
	Spring connection = {
		masspoint1,
		masspoint2,
		initialLength
	};

	this->springs.push_back(connection);
}

int MassSpringSystemSimulator::getNumberOfSprings() {
	return this->springs.size();
}

void MassSpringSystemSimulator::applyExternalForce(Vec3 force) {
	/*Vec3 accel = force / this->m_fMass;
	for (size_t i = 0; i < this->massPoints.size(); i++)
	{
		this->massPoints.at(i).acceleration += accel;
	}*/
}